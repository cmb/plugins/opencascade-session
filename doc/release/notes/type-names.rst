Type names have changed
-----------------------

The node classes in the OpenCascade session now declare their type
names as fully-qualified names (i.e., they all start with ``smtk::session::opencascade::``)
rather than their previous names (which included only the node class name).
This, along with a fix in SMTK for how filter-query strings are parsed, mean
that if you previously used unqualified names, you will need to update the filter
string to use qualified names. Also, as a reminder, type names should be single-quoted.
So, if you want a set of ``Solid`` instances from an OpenCascade resource, you
should now run

.. code:: cpp

   using namespace smtk::session::opencascade;
   auto solids = resource->filterAs<std::set<Solid>>("'smtk::session::opencascade::Solid'");
