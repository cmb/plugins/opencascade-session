# This file is used by SMTK's CI process to ensure that this repo continues
# to be build-able with each SMTK MR. If your SMTK MR causes this contract
# test to fail, you should prepare a separate MR for this repository that fixes
# the issue and merge it as soon as possible after your SMTK MR is merged.

cmake_minimum_required(VERSION 3.20)
project(opencascade-session-contract)

include(ExternalProject)

if (NOT DEFINED SMTK_CONTRACT_TAG)
  set(SMTK_CONTRACT_TAG "origin/master")
endif()

ExternalProject_Add(opencascade-session-contract
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/plugins/opencascade-session.git"
  GIT_TAG "${SMTK_CONTRACT_TAG}"
  PREFIX plugin
  STAMP_DIR plugin/stamp
  SOURCE_DIR plugin/src
  BINARY_DIR plugin/build
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DENABLE_TESTING=ON
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -Dsmtk_DIR=${smtk_DIR}
    ${response_file}
  INSTALL_COMMAND ""
  TEST_BEFORE_INSTALL True
)
