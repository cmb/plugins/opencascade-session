find_package(smtk REQUIRED)
find_package(OpenCASCADE REQUIRED)
find_package(Boost ${SMTK_MINIMUM_BOOST_VERSION} REQUIRED COMPONENTS filesystem)
find_package(nlohmann_json)
