//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Shell_h
#define smtk_session_opencascade_Shell_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class Face;
class Solid;

class SMTKOPENCASCADESESSION_EXPORT Shell : public Shape
{
public:
  smtkTypeMacro(smtk::session::opencascade::Shell);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  Shell(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of solids referencing this shell.
    */
  //@{
  ArcEndpointInterface<SolidsToShells, ConstArc, IncomingArc> solids() const;
  ArcEndpointInterface<SolidsToShells, NonConstArc, IncomingArc> solids();
  //@}

  /**\brief Return the container of faces composing this shell.
    */
  //@{
  ArcEndpointInterface<ShellsToFaces, ConstArc, OutgoingArc> faces() const;
  ArcEndpointInterface<ShellsToFaces, NonConstArc, OutgoingArc> faces();
  //@}
};
}
}
}

#endif
