//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Vertex.h"

#include "smtk/session/opencascade/Edge.h"

namespace smtk
{
namespace session
{
namespace opencascade
{
ArcEndpointInterface<EdgesToVertices, ConstArc, IncomingArc> Vertex::edges() const
{
  return this->incoming<EdgesToVertices>();
}

ArcEndpointInterface<EdgesToVertices, NonConstArc, IncomingArc> Vertex::edges()
{
  return this->incoming<EdgesToVertices>();
}
} // namespace opencascade
} // namespace session
} // namespace smtk
