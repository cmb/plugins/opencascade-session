//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_Traits_h
#define smtk_session_opencascade_Traits_h
/*!\file */

#include "smtk/session/opencascade/Exports.h"
#include "smtk/session/opencascade/arcs/ParentChild.h"

#include <tuple>

namespace smtk
{
namespace session
{
namespace opencascade
{

class CompSolid;
class Compound;
class Edge;
class Face;
class Shape;
class Shell;
class Solid;
class Vertex;
class Wire;

using ParentsToChildren = ParentChild<Shape, Shape>;
using CompoundsToCompSolids = ParentChild<Compound, CompSolid>;
using CompSolidsToSolids = ParentChild<CompSolid, Solid>;
using SolidsToShells = ParentChild<Solid, Shell>;
using ShellsToFaces = ParentChild<Shell, Face>;
using FacesToWires = ParentChild<Face, Wire>;
using WiresToEdges = ParentChild<Wire, Edge>;
using EdgesToVertices = ParentChild<Edge, Vertex>;

/**\brief Traits that describe OpenCASCADE node and arc types.
  *
  */
struct SMTKOPENCASCADESESSION_EXPORT Traits
{
  using NodeTypes = std::tuple<
    Shape
    , Compound
    , CompSolid
    , Solid
    , Shell
    , Face
    , Wire
    , Edge
    , Vertex
    >;
  using ArcTypes = std::tuple<
      ParentsToChildren
    , CompoundsToCompSolids
    , CompSolidsToSolids
    , SolidsToShells
    , ShellsToFaces
    , FacesToWires
    , WiresToEdges
    , EdgesToVertices
  >;
};
}
}
}

// MSVC 2019 requires these since most of the templated
// methods on Resource and files that depend on it are
// parsed speculatively rather than on demand. Bleh.
#include "smtk/session/opencascade/CompSolid.h"
#include "smtk/session/opencascade/Compound.h"
#include "smtk/session/opencascade/Edge.h"
#include "smtk/session/opencascade/Face.h"
#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Shell.h"
#include "smtk/session/opencascade/Solid.h"
#include "smtk/session/opencascade/Vertex.h"
#include "smtk/session/opencascade/Wire.h"

#endif
