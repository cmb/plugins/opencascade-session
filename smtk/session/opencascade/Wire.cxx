//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Wire.h"

#include "smtk/session/opencascade/Edge.h"
#include "smtk/session/opencascade/Face.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Wire::Wire(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<FacesToWires, ConstArc, IncomingArc> Wire::faces() const
{
  return this->incoming<FacesToWires>();
}

ArcEndpointInterface<FacesToWires, NonConstArc, IncomingArc> Wire::faces()
{
  return this->incoming<FacesToWires>();
}

ArcEndpointInterface<WiresToEdges, ConstArc, OutgoingArc> Wire::edges() const
{
  return this->outgoing<WiresToEdges>();
}

ArcEndpointInterface<WiresToEdges, NonConstArc, OutgoingArc> Wire::edges()
{
  return this->outgoing<WiresToEdges>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
