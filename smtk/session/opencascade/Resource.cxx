//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/io/Logger.h"

#include "smtk/session/opencascade/CompSolid.h"
#include "smtk/session/opencascade/Compound.h"
#include "smtk/session/opencascade/Edge.h"
#include "smtk/session/opencascade/Face.h"
#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Shell.h"
#include "smtk/session/opencascade/Solid.h"
#include "smtk/session/opencascade/Vertex.h"
#include "smtk/session/opencascade/Wire.h"
#include "smtk/session/opencascade/queries/SelectionFootprint.h"

#include "smtk/resource/query/Queries.h"

#include <BRep_Builder.hxx>

namespace smtk
{
namespace session
{
namespace opencascade
{

namespace
{
using QueryTypes = std::tuple<SelectionFootprint>;
}

Resource::Resource(const smtk::common::UUID& uid, smtk::resource::ManagerPtr manager)
  : Superclass(uid, manager)
{
  BRep_Builder aBuilder;
  aBuilder.MakeCompound(m_compound);
  this->queries().registerQueries<QueryTypes>();
}

Resource::Resource(smtk::resource::ManagerPtr manager)
  : Superclass(manager)
{
  BRep_Builder aBuilder;
  aBuilder.MakeCompound(m_compound);
  this->queries().registerQueries<QueryTypes>();
}

void Resource::setSession(const Session::Ptr& session)
{
  m_session = session->shared_from_this();
}

smtk::shared_ptr<Shape> Resource::createShape(TopoDS_Shape& shape)
{
  auto shapeType = shape.ShapeType();
  Shape::Ptr node;
  switch (shapeType)
  {
    case TopAbs_COMPOUND:
      node = this->createShape<Compound>();
      break;
    case TopAbs_COMPSOLID:
      node = this->createShape<CompSolid>();
      break;
    case TopAbs_SOLID:
      node = this->createShape<Solid>();
      break;
    case TopAbs_SHELL:
      node = this->createShape<Shell>();
      break;
    case TopAbs_FACE:
      node = this->createShape<Face>();
      break;
    case TopAbs_WIRE:
      node = this->createShape<Wire>();
      break;
    case TopAbs_EDGE:
      node = this->createShape<Edge>();
      break;
    case TopAbs_VERTEX:
      node = this->createShape<Vertex>();
      break;
    case TopAbs_SHAPE: // fall through
    default:
      node = this->createShape<Shape>();
      break;
  }
  return node;
}

} // namespace opencascade
} // namespace session
} // namespace smtk
