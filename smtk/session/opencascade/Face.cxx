//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Face.h"

#include "smtk/session/opencascade/Shell.h"
#include "smtk/session/opencascade/Wire.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Face::Face(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<ShellsToFaces, ConstArc, IncomingArc> Face::shells() const
{
  return this->incoming<ShellsToFaces>();
}

ArcEndpointInterface<ShellsToFaces, NonConstArc, IncomingArc> Face::shells()
{
  return this->incoming<ShellsToFaces>();
}

ArcEndpointInterface<FacesToWires, ConstArc, OutgoingArc> Face::wires() const
{
  return this->outgoing<FacesToWires>();
}

ArcEndpointInterface<FacesToWires, NonConstArc, OutgoingArc> Face::wires()
{
  return this->outgoing<FacesToWires>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
