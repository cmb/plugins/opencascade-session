//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/session/opencascade/Compound.h"
#include "smtk/session/opencascade/CompSolid.h"
#include "smtk/session/opencascade/Edge.h"
#include "smtk/session/opencascade/Face.h"
#include "smtk/session/opencascade/Registrar.h"
#include "smtk/session/opencascade/Resource.h"
#include "smtk/session/opencascade/Session.h"
#include "smtk/session/opencascade/Shell.h"
#include "smtk/session/opencascade/Solid.h"
#include "smtk/session/opencascade/Wire.h"
#include "smtk/session/opencascade/Vertex.h"
#include "smtk/session/opencascade/operators/CreateBox.h"

#include "smtk/common/testing/cxx/helpers.h"

#include "smtk/resource/Manager.h"

#include "smtk/operation/Manager.h"

int TestTraversal(int, char* [])
{
  using namespace smtk::session::opencascade;

  // Create a resource manager
  smtk::resource::Manager::Ptr resourceManager = smtk::resource::Manager::create();

  // Register opencascade resources to the resource manager
  {
    smtk::session::opencascade::Registrar::registerTo(resourceManager);
  }

  // Create an operation manager
  smtk::operation::Manager::Ptr operationManager = smtk::operation::Manager::create();

  // Register opencascade operators to the operation manager
  {
    smtk::session::opencascade::Registrar::registerTo(operationManager);
  }

  // Register the resource manager to the operation manager (newly created
  // resources will be automatically registered to the resource manager).
  operationManager->registerResourceManager(resourceManager);

  // Create a "create box" operator
  smtk::operation::Operation::Ptr createBoxOp =
    operationManager->create<smtk::session::opencascade::CreateBox>();

  if (!createBoxOp)
  {
    std::cerr << "Couldn't create \"create box\" operator" << std::endl;
    return 1;
  }

  createBoxOp->parameters()->findDouble("center")->setValue(0, 0.);
  createBoxOp->parameters()->findDouble("center")->setValue(1, 0.);
  createBoxOp->parameters()->findDouble("center")->setValue(2, 0.);

  createBoxOp->parameters()->findDouble("size")->setValue(0, 1.);
  createBoxOp->parameters()->findDouble("size")->setValue(1, 1.);
  createBoxOp->parameters()->findDouble("size")->setValue(2, 1.);

  smtk::operation::Operation::Result createBoxOpResult = createBoxOp->operate();

  if (createBoxOpResult->findInt("outcome")->value() !=
    static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
  {
    std::cerr << "\"create box\" operator failed\n";
    return 1;
  }

  smtk::attribute::ResourceItemPtr resourceItem = createBoxOpResult->findResource("resource");

  smtk::session::opencascade::Resource::Ptr resource =
    std::dynamic_pointer_cast<smtk::session::opencascade::Resource>(resourceItem->value());

  std::cout << "There are " << resource->nodes().size() << " nodes" << std::endl;

  auto solids =
    resource->filterAs<std::set<std::shared_ptr<Solid>>>("'smtk::session::opencascade::Solid'");
  std::cout << solids.size() << " solids\n";
  ::test(solids.size() == 1, "Expected a single solid out.");

  for (const auto& solid : solids)
  {
    std::cout << "solid " << solid.get() << "\n";
    solid->compsolids().visit([](const CompSolid* compSolid)
      {
        std::cout << "  compSolid " << compSolid << "\n";
      }
    );
    int shellCount = 0;
    solid->shells().visit([&shellCount](const Shell* shell)
      {
        std::cout << "  shell " << shell << ", " << shell->solids().degree() << " solids\n";
        ++shellCount;
        ::test(shell->solids().degree() == 1, "Expected 1 solid parent of shell.");
        int faceCount = 0;
        shell->faces().visit([&faceCount](const Face* face)
          {
            std::cout << "    face " << face << ", " << face->shells().degree() << " shells.\n";
            ++faceCount;
            ::test(face->shells().degree() == 1, "Expected 1 shell parent of face.");
            int wireCount = 0;
            face->wires().visit([&wireCount](const Wire* wire)
              {
                std::cout << "      wire " << wire << ", " << wire->faces().degree() << " faces.\n";
                ++wireCount;
                ::test(wire->faces().degree() == 1, "Expected 1 face parent of wire.");
                int edgeCount = 0;
                wire->edges().visit([&edgeCount](const Edge* edge)
                  {
                    std::cout
                      << "        edge " << edge << ", " << edge->wires().degree() << " wires.\n";
                    ++edgeCount;
                    ::test(edge->wires().degree() == 2, "Expected 2 wire parents of edge.");
                    int vertCount = 0;
                    edge->vertices().visit([&vertCount](const Vertex* vert)
                      {
                        std::cout
                          << "          vertex " << vert << ", "
                          << vert->edges().degree() << " edges.\n";
                        ++vertCount;
                        // We expect 6 edges because the parents are really oriented co-edges:
                        ::test(vert->edges().degree() == 6, "Expected 6 edge parents of vertex.");
                      }
                    );
                    ::test(vertCount == 2, "Expected 2 vertices.");
                  }
                );
                ::test(edgeCount == 4, "Expected 4 edges.");
              }
            );
            ::test(wireCount == 1, "Expected 1 wires.");
          }
        );
        ::test(faceCount == 6, "Expected 6 faces.");
      }
    );
    ::test(shellCount == 1, "Expected 1 shell.");
  }

  return 0;
}
