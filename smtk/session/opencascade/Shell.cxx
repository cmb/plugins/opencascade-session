//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/session/opencascade/Shell.h"

#include "smtk/session/opencascade/Face.h"
#include "smtk/session/opencascade/Solid.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

Shell::Shell(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc)
  : Shape(rsrc)
{
}

ArcEndpointInterface<SolidsToShells, ConstArc, IncomingArc> Shell::solids() const
{
  return this->incoming<SolidsToShells>();
}

ArcEndpointInterface<SolidsToShells, NonConstArc, IncomingArc> Shell::solids()
{
  return this->incoming<SolidsToShells>();
}

ArcEndpointInterface<ShellsToFaces, ConstArc, OutgoingArc> Shell::faces() const
{
  return this->outgoing<ShellsToFaces>();
}

ArcEndpointInterface<ShellsToFaces, NonConstArc, OutgoingArc> Shell::faces()
{
  return this->outgoing<ShellsToFaces>();
}

} // namespace opencascade
} // namespace session
} // namespace smtk
