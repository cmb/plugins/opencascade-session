//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_CompSolid_h
#define smtk_session_opencascade_CompSolid_h

#include "smtk/session/opencascade/Shape.h"
#include "smtk/session/opencascade/Traits.h"

namespace smtk
{
namespace session
{
namespace opencascade
{

class Compound;
class Solid;

class SMTKOPENCASCADESESSION_EXPORT CompSolid : public Shape
{
public:
  smtkTypeMacro(smtk::session::opencascade::CompSolid);
  smtkSuperclassMacro(smtk::session::opencascade::Shape);

  CompSolid(const std::shared_ptr<smtk::graph::ResourceBase>& rsrc);

  /**\brief Return the container of compounds referencing this compsolid.
    */
  //@{
  ArcEndpointInterface<CompoundsToCompSolids, ConstArc, IncomingArc> compounds() const;
  ArcEndpointInterface<CompoundsToCompSolids, NonConstArc, IncomingArc> compounds();
  //@}

  /**\brief Return the container of solids composing this compsolid.
    */
  //@{
  ArcEndpointInterface<CompSolidsToSolids, ConstArc, OutgoingArc> solids() const;
  ArcEndpointInterface<CompSolidsToSolids, NonConstArc, OutgoingArc> solids();
  //@}
};
}
}
}

#endif
