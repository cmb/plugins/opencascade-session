//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_session_opencascade_ParentChild_h
#define smtk_session_opencascade_ParentChild_h

#include "smtk/graph/Resource.h"
#include "smtk/session/opencascade/Shape.h"

#include "smtk/common/CompilerInformation.h"

#include "TopoDS_Iterator.hxx"
#include "TopoDS_Shape.hxx"
#include "TopExp.hxx"

namespace smtk
{
namespace session
{
namespace opencascade
{

template<typename XParentType, typename XChildType>
class SMTK_ALWAYS_EXPORT ParentChild
{
public:
  using FromType = XParentType;
  using ToType = XChildType;
  using Directed = std::true_type;
  // using Mutable = std::false_type;

  template<typename Functor>
  smtk::common::Visited outVisitor(const FromType* component, Functor&& ff) const;

  template<typename Functor>
  smtk::common::Visited inVisitor(const ToType* component, Functor&& ff) const;
};

}
}
}

#endif
